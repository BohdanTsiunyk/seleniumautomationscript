﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Opera;

namespace SeleniumAutomationScript
{
    class TestCase
    {
        static void Main(string[] args)
        {
            string path = @"C:\Users\Bodia\source\repos\SeleniumAutomationScript\SeleniumAutomationScript";
            int letterCounter = 10;
            IWebDriver operaDriver = new OperaDriver(path)
            {
                Url = "http://www.google.com"
            };

            RandomWord randomWord = new RandomWord();
            var query = randomWord.GenerateRandomWord(letterCounter);

            IWebElement SearchInput = operaDriver.FindElement(By.ClassName("gLFyf gsfi"));
            SearchInput.SendKeys(query);

            IWebElement buttomSearch = operaDriver.FindElement(By.ClassName("gNO89b"));
            buttomSearch.Click();

            IWebElement myTitle = operaDriver.FindElement(By.TagName("title"));

            Console.WriteLine(myTitle);
        }

    }
}
