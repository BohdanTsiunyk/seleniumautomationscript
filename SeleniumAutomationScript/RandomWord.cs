﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SeleniumAutomationScript
{
   public class RandomWord : Random
    {
        public const string alphabet = "ABCDEFGHIJKLMNOPQRSTUVXZYW";
        public static Random random = new Random();

        internal string GenerateRandomWord(int numberOfLetters)
        {
            var word = "";
            for (var i = 0; i < numberOfLetters; i++)
            {
                word += GenerateRandomLetter();
            }
            return word;
        }

        private static string GenerateRandomLetter()
        {
            int n = random.Next(0, alphabet.Length);

            return alphabet[n].ToString();
        }
    }
}
